#!/usr/bin/make -f
# -*- makefile -*-

#export DH_VERBOSE=1

export DEB_BUILD_MAINT_OPTIONS=hardening=+all # ,-pie

include /usr/share/dpkg/pkg-info.mk
# SOURCE_DATE_EPOCH ?= $(shell date -d "$$(dpkg-parsechangelog -S Date)" +%s)
BUILD_DATE=$(shell LC_ALL=C date --utc --date="@$(SOURCE_DATE_EPOCH)" "+%B %d, %Y")
SPHINXOPTS := -D today="$(BUILD_DATE)"
SPHINXOPTS += -D html_last_updated_fmt="$(BUILD_DATE)"

ifneq ($(wildcard /usr/lib/$(DEB_HOST_MULTIARCH)/hdf5/serial/libhdf5.so),)
  export HDF5_DIR := /usr/lib/$(DEB_HOST_MULTIARCH)/hdf5/serial
endif

export DISABLE_AVX2=1 DISABLE_SSE2=1


%:
	dh $@ --with python3,sphinxdoc --buildsystem=pybuild


override_dh_auto_build: export http_proxy=127.0.0.1:9
override_dh_auto_build: export https_proxy=127.0.0.1:9
override_dh_auto_build:
	$(MAKE) ANNOUNCE.txt
	dh_auto_build --buildsystem=pybuild
ifeq (,$(filter nodoc,$(DEB_BUILD_OPTIONS)))
	cd $(CURDIR)/.pybuild/cpython3_$(shell py3versions -vd)/build && \
		env PYTHONPATH=. python3 -m sphinx -M html $(CURDIR)/doc/source $(CURDIR)/doc/build -N $(SPHINXOPTS)
	cd $(CURDIR)/.pybuild/cpython3_$(shell py3versions -vd)/build && \
		env PYTHONPATH=. python3 -m sphinx -M latexpdf $(CURDIR)/doc/source $(CURDIR)/doc/build -N $(SPHINXOPTS)
	mv doc/build/html doc/html
	mv doc/build/latex/usersguide-*.pdf doc/usersguide.pdf
else
	mkdir -p doc/html
	touch doc/usersguide.pdf
endif


override_dh_python3:
	dh_python3
	dh_numpy3


override_dh_strip:
	dh_strip -ppython3-tables-lib


override_dh_auto_test:
ifeq (,$(filter nocheck,$(DEB_BUILD_OPTIONS)))
	PYBUILD_SYSTEM=custom \
	PYBUILD_TEST_ARGS="cd {build_dir} && env PYTHONPATH=. python{version} -m unittest -v tables.tests.test_all.suite" \
	dh_auto_test
endif


override_dh_auto_clean:
	# Avoid unnecessary cythonization (see
	# https://github.com/PyTables/PyTables/issues/122)
	#dh_auto_clean --buildsystem=pybuild
	$(MAKE) distclean
	$(RM) -r doc/*.pdf tables.egg-info tmp-locales
	$(RM) -r .pybuild


override_dh_installchangelogs:
	dh_installchangelogs -k RELEASE_NOTES.rst


override_dh_compress:
	dh_compress -X.txt -X.pdf -X.py -X.h5
